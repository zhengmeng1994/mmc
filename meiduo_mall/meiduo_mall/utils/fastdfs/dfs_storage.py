# 自定义一个Django文件保存在fastdfs服务器上,需要继承于Storage
from django.conf import settings
from django.core.files.storage import Storage
from fdfs_client.client import Fdfs_client


class FastDFSStorage(Storage):
    # 将文件保存到FastDFS中
    def __init__(self, base_url=None, client_conf=None):
        # 判断文件地址是否存在,不存在则设置dev中的文件保存地址
        if base_url is None:
            base_url = settings.FDFS_URL
        self.base_url = base_url
        # 设置使用 dev中的FastDFS客户端的配置
        if client_conf is None:
            client_conf = settings.FDFS_CLIENT_CONF
        self.client_conf = client_conf

    # 自定义文件储存中必须带有open和save方法
    def _open(self, name, mode='rb'):
        # 不需要用到打开文件
        pass

    def _save(self, name, content):
        """
               在FastDFS中保存文件
               :param name: 传入的文件名
               :param content: 文件内容
               :return: 保存到数据库中的FastDFS的文件名
               """
        client = Fdfs_client(self.client_conf)
        ret = client.upload_by_buffer(content.read())
        if ret.get("Status") != "Upload successed.":
            raise Exception("upload file failed")
        file_name = ret.get("Remote file_id")
        return file_name

    # 定义文件保存地址的方法
    def url(self, name):
        """
        返回文件的完整URL路径
        :param name: 数据库中保存的文件名
        :return: 完整的URL
        """
        return self.base_url + name

    # 定义文件是否存在的方法
    def exists(self, name):
        """
        判断文件是否存在，FastDFS可以自行解决文件的重名问题
        所以此处返回False，告诉Django上传的都是新文件
        :param name:  文件名
        :return: False
        """
        return False
