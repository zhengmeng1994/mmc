from _decimal import Decimal
from time import timezone

from django.db import transaction
from django_redis import get_redis_connection
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from goods.models import SKU

# 定义一个获取购物车商品数据等待结算的序列化器
from meiduo_mall.utils.exceptions import logger
from orders.models import OrderInfo, OrderGoods


class OrderSkuSerializer(serializers.ModelSerializer):
    # 定义需要校验的字段属性
    count = serializers.IntegerField(label='数量')

    # 定义校验的模型类以及需要返回给前端的字段
    class Meta:
        model = SKU
        fields = ['id', 'name', 'default_image_url', 'price', 'count']


# 定义一个订单结算的序列化器
class OrderSettlementSerializer(serializers.Serializer):
    """
    订单结算数据序列化器
    """
    freight = serializers.DecimalField(label='运费', max_digits=10, decimal_places=2)
    # 创建序列化器对象,生成订单结算
    skus = OrderSkuSerializer(many=True)


# 定义个保存订单的序列化器
class SaveOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderInfo
        fields = ('order_id', 'address', 'pay_method')
        read_only_fields = ('order_id',)
        extra_kwargs = {
            'address': {
                'write_only': True,
                'required': True,
            },
            'pay_method': {
                'write_only': True,
                'required': True
            }
        }

    # 定义保存订单的方法
    def create(self, validated_data):
        # 获取当前下单用户
        user = self.context['request'].user
        # 生成订单编号
        order_id = timezone.now().strftime('%Y%m%d%H%M%S') + ('%09d' % user.id)
        address = validated_data['address']
        pay_method = validated_data['pay_method']
        # 保存订单基本信息数据 OrderInfo
        # 生成订单
        with transaction.atomic():
            # 创建一个保存点
            save_id = transaction.savepoint()

            try:
                # 创建订单信息
                order = OrderInfo.objects.create(
                    order_id=order_id,
                    user=user,
                    address=address,
                    total_count=0,
                    total_amount=Decimal(0),
                    freight=Decimal(10),
                    pay_method=pay_method,
                    status=OrderInfo.ORDER_STATUS_ENUM['UNSEND'] if pay_method == OrderInfo.PAY_METHODS_ENUM[
                        'CASH'] else OrderInfo.ORDER_STATUS_ENUM['UNPAID']
                )
                # 从redis中获取购物车结算商品数据
                redis_conn = get_redis_connection("cart")
                redis_cart = redis_conn.hgetall("cart_%s" % user.id)
                cart_selected = redis_conn.smembers('cart_selected_%s' % user.id)
                # 将bytes类型转换为int类型
                cart = {}
                for sku_id in cart_selected:
                    cart[int(sku_id)] = int(redis_cart[sku_id])

                # 一次查询出所有商品数据
                skus = SKU.objects.filter(id__in=cart.keys())

                # 遍历结算商品：
                for sku in skus:
                    sku_count = cart[sku.id]
                    # 判断商品库存是否充足
                    origin_stock = sku.stock  # 原始库存
                    origin_sales = sku.sales  # 原始销量
                    # 如果商品下单数量大于商品库存数量,则下单不成功返回之前的未下单的状态,同时提示用户商品库存不足
                    if sku_count > origin_stock:
                        transaction.savepoint_rollback(save_id)
                        raise serializers.ValidationError('商品库存不足')
                    # 减少商品库存，增加商品销量
                    new_stock = origin_stock - sku_count
                    new_sales = origin_sales + sku_count
                    # 根据原始库存条件更新，返回更新的条目数，乐观锁
                    ret = SKU.objects.filter(id=sku.id, stock=origin_stock).update(stock=new_stock, sales=new_sales)
                    if ret == 0:
                        continue
                    # sku.stock = new_stock
                    # sku.sales = new_sales
                    # sku.save()

                    # 累计商品的SPU 销量信息
                    sku.goods.sales += sku_count
                    sku.goods.save()
                    # 累计订单基本信息的数据
                    order.total_count += sku_count  # 累计总金额
                    order.total_amount += (sku.price * sku_count)  # 累计总额
                    # 保存订单商品数据
                    # 保存订单商品
                    OrderGoods.objects.create(
                        order=order,
                        sku=sku,
                        count=sku_count,
                        price=sku.price,
                    )
                # 更新订单的金额数量信息
                order.total_amount += order.freight
                order.save()
            except ValidationError:
                raise
            except Exception as e:
                logger.error(e)
                transaction.savepoint_rollback(save_id)
                raise
            # 提交事务
            transaction.savepoint_commit(save_id)

            # 更新redis中保存的购物车数据
            pl = redis_conn.pipeline()
            pl.hdel('cart_%s' % user.id, *cart_selected)
            pl.srem('cart_selected_%s' % user.id, *cart_selected)
            pl.execute()
            return order