from django.conf.urls import url

from orders import views

urlpatterns = [
    url(r'^orders/settlement/$', views.OrderView.as_view()),
    url(r'^orders/$', views.SaveOrder.as_view()),
]
