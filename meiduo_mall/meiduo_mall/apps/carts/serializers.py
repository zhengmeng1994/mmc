from rest_framework import serializers

# 定义一个购物车序列化器
from goods.models import SKU


class CartSerializer(serializers.Serializer):
    # 定义需要校验的数据,商品信息,商品数量以及商品是否被勾选
    sku_id = serializers.IntegerField(label='sku_id', min_value=1)
    count = serializers.IntegerField(label='数量', min_value=1)
    selected = serializers.BooleanField(label='是否勾选', default=True)

    # 定义校验商品数据是否存在的方法
    def validate(self, attrs):
        # 可能获取不到数据,放入异常捕获中
        try:
            sku = SKU.objects.get(id=attrs['sku_id'])
        except SKU.DoesNotExist:
            raise serializers.ValidationError('商品不存在')

        return attrs


# 定义一个查询购物车中商品的序列化器
class CartSKUSerializer(serializers.ModelSerializer):
    # 定义需要校验的字段属性,商品信息和数量是是否勾选
    count = serializers.IntegerField(label='数量')
    selected = serializers.BooleanField(label='是否勾选')

    # 关联的模型类对象
    class Meta:
        model = SKU
        fields = ('id', 'count', 'name', 'default_image_url', 'price', 'selected')


# 定义一个删除购物车中商品数据的序列化器
class CartDeleteSerializer(serializers.Serializer):
    sku_id = serializers.IntegerField(label='商品id', min_value=1)

    # 定义一个校验商品是否存在的方法
    def validate_sku_id(self, value):
        try:
            sku = SKU.objects.get(id=value)
        except SKU.DoesNotExist:
            raise serializers.ValidationError('商品不存在')

        return value


# 定义一个全选购物车商品的序列化器
class CartSelectAllSerializer(serializers.Serializer):
    """
    购物车全选
    """
    selected = serializers.BooleanField(label='全选')
