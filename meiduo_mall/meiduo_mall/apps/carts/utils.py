# 定义一个登录合并购物车的函数
import base64
import pickle

from django_redis import get_redis_connection


def merge_shopping_cart(request,user,response):
    """
        合并请求用户的购物车数据，将未登录保存在cookie里的保存到redis中
        遇到cookie与redis中出现相同的商品时以cookie数据为主，覆盖redis中的数据
        :param request: 用户的请求对象
        :param user: 当前登录的用户
        :param response: 响应对象，用于清楚购物车cookie
        :return:
    """
    # 用户未登录,获取请求对象中的商品数据,是字符串类型
    cookie_cart = request.COOKIES.get('cart')
    # 判断是否获取到购物车数据,没有直接返回响应对象
    if not cookie_cart:
        return response
    # 对获取到的购物车数据解密,解密后的类型是字典
    cart = pickle.loads(base64.b64decode(cookie_cart.encode()))
    # 定义一个存放商品信息以及数量的字典还有存放勾选状态的列表
    new_cart = {}
    # 记录redis勾选状态中应该增加的sku_id
    redis_cart_selected_add = []
    # 记录redis勾选状态中应该删除的sku_id
    redis_cart_selected_remove = []
    # 合并cookie购物车与redis购物车，保存到cart字典中
    #  {'1': {'count': 10, 'selected': True},'2': {'count': 20, 'selected': True}}
    for sku_id, count_selected_dict in cart.items():
        # 处理商品数量,将商品id和数量以键值对的形式存入空字典中
        new_cart[sku_id] = count_selected_dict['count']
        # 判断购物车中的商品是否勾选,勾选的商品就把商品id放入已经勾选的列表中
        if count_selected_dict['selected']:
            redis_cart_selected_add.append(sku_id)
        else:
        # 如果不是勾选状态则将该商品放入需要移除的列表中
            redis_cart_selected_remove.append(sku_id)

    # 判断字典里的数据是否存在
    if new_cart:
        # 同步数据到用户登录的购物车中
        redis = get_redis_connection('cart')
        # 统一操作
        pl = redis.pipeline()
        # 如果增加商品的列表中存在数据,就将商品的数据存入redis数据库中
        if redis_cart_selected_add:
            pl.sadd('cart_selected_%s' % user.id, *redis_cart_selected_add)
        # 如果移除商品的列表中有数据,则将redis数据库中相同的商品移除
        if redis_cart_selected_remove:
            pl.srem('cart_selected_%s' % user.id, *redis_cart_selected_remove)
        pl.execute()
        # 删除cookie中的购物车数据
    response.delete_cookie('cart')
    return response
