# 配置导航栏以及商品列表数据的显示的路由
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from goods import views

urlpatterns = [
    # 导航栏的路由地址
    url(r'^categories/(?P<pk>\d+)/$', views.CategoryView.as_view()),
    # 商品列表数据的显示
    url(r'^categories/(?P<category_id>\d+)/skus/$', views.SKUListView.as_view())
]
router = DefaultRouter()
router.register('skus/search', views.SKUSearchViewSet, base_name='skus_search')


urlpatterns += router.urls
