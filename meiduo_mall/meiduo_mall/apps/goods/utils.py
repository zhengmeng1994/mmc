from collections import OrderedDict

from goods.models import GoodsChannel


def get_categories():
    categories = OrderedDict()
    # 首先查询一级分组
    # 获取商品分类数据,按照组号以及组内顺序
    channels = GoodsChannel.objects.order_by('group_id', 'sequence')
    # print(channels)
    # 遍历获取到的商品分类数据,获取当前的组
    for channel in channels:
        group_id = channel.group_id  # 当前组
        # 将当前组放入有序字典中
        if group_id not in categories:
            categories[group_id] = {'channels': [], 'sub_cats': []}
        # 第一个分组是当前频道的类别
        cat1 = channel.category  # 当前频道的类别
        # 追加当前频道,将查询到的数据放入有序字典中
        categories[group_id]['channels'].append({
            'id': cat1.id,
            'name': cat1.name,
            'url': channel.url
        })
        print(categories)

        # 查询第二级分类
        # 构建当前类别的子类别,通过第一级分类关联的二级分类id查询下面所有的二级分类
        for cat2 in cat1.goodscategory_set.all():
            # 定义一个空列表用来储存其下三级分类
            cat2.sub_cats = []
            # 通过二级分类关联的三级分类id查询到下面的所有三级分类
            for cat3 in cat2.goodscategory_set.all():
                # 将所有的三级分类的添加到二级分类中
                cat2.sub_cats.append(cat3)

            categories[group_id]['sub_cats'].append(cat2)
    return categories