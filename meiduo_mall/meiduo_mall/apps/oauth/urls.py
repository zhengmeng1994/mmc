from django.conf.urls import url

from oauth import views

urlpatterns = [
    url(r'^oauth/qq/authorization/$', views.QQloginaView.as_view()),
    url(r'^oauth/qq/user/$', views.UserBindQQ.as_view()),
]
