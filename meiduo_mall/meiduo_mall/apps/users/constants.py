# 设置邮件过期时间
EMAIL_OUTTIME = 2 * 24 * 60 * 60
# 允许用户设置收货地址的最大数量
USER_ADDRESS_COUNTS_LIMIT = 10
# 用户能查看的最大的商品浏览历史记录数
USER_HISTORY_COUNTS = 5
