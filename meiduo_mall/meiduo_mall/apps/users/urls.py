# 定义用户注册的访问路由
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token

from users import views

urlpatterns = [
    url(r'^usernames/(?P<username>\w{5,20})/count/$', views.UserNameCount.as_view()),
    url(r'^mobiles/(?P<mobile>1[3-9]\d{9})/count/$', views.MobileCount.as_view()),
    url(r'^users/$', views.UserAPIView.as_view()),
    # jwt自带了用户登录的接口应用,但是需要我们添加返回的数据
    # url(r'^authorizations/$', obtain_jwt_token),
    url(r'^authorizations/$', views.UserAuthorizeView.as_view()),
    url(r'^user/$', views.UserDetail.as_view()),
    # 设置邮件保存以及验证的路由
    url(r'^email/$', views.SaveEmail.as_view()),
    url(r'^emails/verification/$', views.SendEmail.as_view()),
    # 设置用户浏览历史记录的路由地址
    url(r'^browse_histories/$', views.UserHistoryView.as_view())
]

router = DefaultRouter()
router.register('addresses', views.UserAddressViewSet, 'address')
urlpatterns += router.urls
