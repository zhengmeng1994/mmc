# 定义一个用户注册的序列化器
import re

from django_redis import get_redis_connection
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.serializers import ModelSerializer

from goods.models import SKU
from users import constants
from users.models import User, Address
from users.utils import generate_save_user_token_url


# 定义一个用户登录认证的序列化器
class UserSerializer(serializers.ModelSerializer):
    # 增加表中需要验证的字段以及属性,密码,同意用户协议以及短信验证码
    password2 = serializers.CharField(label='确认密码', write_only=True)
    allow = serializers.CharField(label='同意用户协议', write_only=True)
    sms_code = serializers.CharField(label='短信验证码', write_only=True)

    # 定义用户表的需要获取的属性
    # 关联用户的模型
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'password2', 'mobile', 'sms_code', 'allow']
        # 修改表中字段的属性
        extra_kwargs = {
            # 用户名增加字段长度的限制
            'username': {
                'min_length': 5,
                'max_length': 20,
                'error_messages': {
                    'min_length': '用户名长度不得小于5个字符',
                    'max_length': '用户名长度不得超过20个字符',
                }
            },
            # 密码增加字符长度的限制
            'password': {
                'min_length': 8,
                'max_length': 20,
                'error_messages': {
                    'min_length': '密码长度不得小于8个字符',
                    'max_length': '密码长度不得超过20个字符',
                }
            },
            #

        }

    # 定义一个验证手机号格式是否正确以及是否已经注册的方法
    def validate_mobile(self, data):
        # 采用正则匹配手机号,不满足抛出异常信息
        if not re.match(r'^1[3-9]\d{9}$', data):
            raise serializers.ValidationError('手机号格式错误')
        # 需要判断手机号是否注册
        count = User.objects.filter(mobile=data).count()
        if count > 0:
            raise serializers.ValidationError('该手机号已经注册')
        # 判断完后需要返回数据
        return data

    # 定义个用户是否同意协议的方法
    def validate_allow(self, data):
        # 判断用户是否同意了协议,没有返回异常信息
        if data != 'true':
            raise serializers.ValidationError('请同意用户协议')
        return data

    # 定义一个短信验证码以及用户输入的密码是否正确的方法
    def validate(self, attrs):
        # 获取用户输入的短信验证码
        sms_code = attrs.get('sms_code')
        # 根据手机号在redis数据库中获取到短信验证码,可能会出现异常
        mobile = attrs.get('mobile')
        # 连接redis数据库
        redis = get_redis_connection('verify')
        try:
            real_code = redis.get('sms_%s' % mobile)
        except:
            raise serializers.ValidationError('验证码已过期或不存在')
        real_code = real_code.decode()
        if sms_code != real_code:
            raise serializers.ValidationError('验证码输入错误')

        # 判断用户输入的两次密码是否正确
        password = attrs.get('password')
        password2 = attrs.get('password2')
        if password != password2:
            raise serializers.ValidationError('两次输入的密码不一致')

        return attrs

    # 重写保存用户的方法
    def create(self, validated_data):
        # 需要去掉短信验证码,第二次输入的密码以及用户同意的协议
        del validated_data['sms_code']
        del validated_data['password2']
        del validated_data['allow']
        # 对密码进行加密,实例化创建用户的对像
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        # 注册成功了，使用jwt保存登陆状态
        from rest_framework_jwt.settings import api_settings

        # 获取 生成载荷的函数
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        # 获取 生成token的函数
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        user.save()
        return user


# 定义邮箱验证的序列化器
class EmailSeralizer(serializers.ModelSerializer):
    # 添加需要验证的字段属性
    class Meta:
        # 声明关联的用户表
        model = User
        # 需要显示的字段名称
        fields = ['id', 'email']
        # 设置字段新的属性选项
        extra_kwargs = {
            'email': {
                'required': True
            }
        }

    # 定义一个修改绑定邮箱的方法
    def update(self, instance, validated_data):
        instance.email = validated_data['email']
        print(instance.email)
        instance.save()
        # 生成验证邮箱的链接[instance 就是当前已经登陆的用户]
        verify_url = generate_save_user_token_url(instance)
        from celery_tasks.email.tasks import send_email
        # 异步发送邮件
        send_email.delay(instance.email, verify_url)
        return instance


# 定义一个添加收货地址的序列化器
class UserAddressSerializer(ModelSerializer):
    # 需要验证的地址中字段属性
    province = serializers.StringRelatedField(read_only=True)
    city = serializers.StringRelatedField(read_only=True)
    district = serializers.StringRelatedField(read_only=True)
    # 后端使用,在数据库中查询对应的行政区划
    province_id = serializers.IntegerField(label='省ID', required=True)
    city_id = serializers.IntegerField(label='市ID', required=True)
    district_id = serializers.IntegerField(label='区ID', required=True)

    class Meta:
        model = Address
        exclude = ('user', 'is_deleted', 'create_date', 'update_time')

    def validate_mobile(self, value):
        """
        验证手机号
        """
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise serializers.ValidationError('手机号格式错误')
        return value

    def create(self, validated_data):
        """
        保存
        """
        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)


# 定义一个显示用户浏览历史的序列化器
class UserHistorySerializer(serializers.Serializer):
    # 定义需要检验的字段属性
    sku_id = serializers.IntegerField(label='商品SKU编号', min_value=1)

    # 定义校验商品SKu编号是否存在的方法
    def validate_sku_id(self, value):
        # 放入异常捕获中
        try:
            SKU.objects.get(id=value)
        except SKU.DoesNotExist:
            raise serializers.ValidationError('该商品不存在')
        return value

    # 重新保存商品浏览记录的方法
    def create(self, validated_data):
        # 获取用户的id以及浏览的商品的id
        user_id = self.context['request'].user.id
        sku_id = validated_data['sku_id']
        # 连接redis数据库,将其存放在数据库中
        redis = get_redis_connection('history')
        # 统一提交操作
        pl = redis.pipeline()
        # 移除已经存在的商品浏览记录
        pl.lrem('history_%s' % user_id, 0, sku_id)
        # 添加新的浏览记录
        pl.lpush('history_%s' % user_id, sku_id)
        # 最多只能查看的历史记录
        pl.ltrim('history_%s' % user_id, 0, constants.USER_HISTORY_COUNTS - 1)
        pl.execute()
        return validated_data
