import os
import time

# 自定义生成静态的前端页面的函数
from collections import OrderedDict

from django.conf import settings
from django.template import loader

from advertising.models import ContentCategory
from goods.models import GoodsChannel
from goods.utils import get_categories


def generate_static_index_html():
    # 生成静态页面的时间
    print('%s: generate_static_index_html' % time.ctime())
    # 获取页面需要的数据
    # 商品频道及分类菜单
    # 使用有序字典保存类别的顺序
    categories = get_categories()
    # print(categories)

    # 获取广告的数据
    # 广告内容
    contents = OrderedDict()
    content_categories = ContentCategory.objects.all()
    for cat in content_categories:
        contents[cat.key] = cat.content_set.filter(status=True).order_by('sequence')

    # 渲染模板
    context = {
        'categories': categories,
        'contents': contents
    }
    template = loader.get_template('index.html')
    # 保存页面内容到index前端页面中
    html_text = template.render(context)
    file_path = os.path.join(settings.GENERATED_STATIC_HTML_FILES_DIR, 'index.html')
    with open(file_path, 'w', encoding='utf-8') as f:
        f.write(html_text)
