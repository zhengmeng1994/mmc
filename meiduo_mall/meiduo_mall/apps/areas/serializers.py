# 定义一个序列化器.用来处理地址设置
from .models import Area
from rest_framework import serializers


# 定义行政区划信息序列化器
class AreasSerializer(serializers.ModelSerializer):
    # 定义关联的模型
    class Meta:
        model = Area
        fields = ['id', 'name']


class SubAreasSerializer(serializers.ModelSerializer):
    # 添加需要返回的新的字段名
    subs = AreasSerializer(many=True, read_only=True)

    # 定义关联的模型
    class Meta:
        model = Area
        fields = ['id', 'name', 'subs']
